package com.trumpetx.minerstatus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdView;
import com.trumpetx.minerstatus.beans.Result;
import com.trumpetx.minerstatus.beans.Status;
import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.beans.Ticker;
import com.trumpetx.minerstatus.beans.TickerMetadata;
import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.service.ThemeService;
import com.trumpetx.minerstatus.tasks.AsynchMinerUpdateTask;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.Metadata;
import com.trumpetx.minerstatus.util.ServiceFactory;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainMinerActivity
    extends AbstractMinerStatusActivity
{
    MinerService _minerService;

    ThemeService _themeService;

    Configuration _configuration;

    private static final String tag = "TX";

    private boolean hasNetworkConnection()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if ( netInfo != null && netInfo.isConnected() )
        {
            return true;
        }
        return false;
    }

    @Override
    protected void onCreateImpl( Bundle savedInstanceState )
    {
        _minerService = getFactory().getMinerService();
        _themeService = getFactory().getThemeService();
        _configuration = getFactory().getConfiguration();

        setContentView( R.layout.main );

        int bgColor = _themeService.getTheme().getBackgroundColor();

        ScrollView scrollView = (ScrollView) findViewById( R.id.mainMinerScrollView );
        scrollView.setBackgroundColor( bgColor );
        displayUserStatusUpdate();

        fetchStatusUpdate();
    }

    private void fetchStatusUpdate()
    {
        if ( !hasNetworkConnection() )
        {
            setTitle( "Miner Status - No Data Connection" );
            Toast.makeText( this, "Please turn on 3/4G or Wifi", Toast.LENGTH_LONG ).show();
        }
        else
        {
            setTitle( "Miner Status - Updating..." );
            new MyAsynchMinerUpdateTask( getFactory() ).execute();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        AdView adView = (AdView) findViewById( R.id.ad );
        if ( adView != null )
        {
            adView.setVisibility( _configuration.getShowAdvertisements() ? AdView.VISIBLE : AdView.INVISIBLE );
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.layout.miner_menu, menu );
        return true;
    }

    static final int REQUESTCODE_ADDMINER = 1;

    static final int REQUESTCODE_OPTIONS = 2;

    static final int REQUESTCODE_VIEWMINER = 3;

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() )
        {
            case R.id.add_miner:
                if ( !hasNetworkConnection() )
                {
                    setTitle( "Miner Status - No Data Connection" );
                    Toast.makeText( this, "Please turn on 3/4G or Wifi", Toast.LENGTH_LONG ).show();
                }
                else
                {
                    startActivityWithFactory( REQUESTCODE_ADDMINER, AddMinerActivity.class );
                }
                break;
            case R.id.fetch_status:
                fetchStatusUpdate();
                break;
            case R.id.options:
                startActivityWithFactory( REQUESTCODE_OPTIONS, OptionsActivity.class );
                break;
            case R.id.about:
                String message = "Maintained by TrumpetX & logiclrd";
                try
                {
                    message += "\nVersion: " + getPackageManager().getPackageInfo( getPackageName(), 0 ).versionName;
                }
                catch ( PackageManager.NameNotFoundException e )
                {
                    // Leave off version info
                }
                Toast.makeText( this, message, Toast.LENGTH_LONG ).show();
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        switch ( requestCode )
        {
            case REQUESTCODE_ADDMINER:
                fetchStatusUpdate();
                break;
            case REQUESTCODE_OPTIONS:
            case REQUESTCODE_VIEWMINER: // miner may be deleted
                displayUserStatusUpdate();
                break;
        }
    }

    @SuppressWarnings( "deprecation" )
    private TableRow createNewRow( String[] columns, Boolean headerTextColor )
    {
        TableRow tr = new TableRow( getApplicationContext() );
        tr.setLayoutParams( new LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT ) );
        for ( String str : columns )
        {
            TextView col = new TextView( getApplicationContext() );
            col.setPadding( getDip( 5F ), getDip( 5F ), getDip( 5F ), getDip( 5F ) );
            col.setTextColor( ( headerTextColor ) ? _themeService.getTheme().getHeaderTextColor()
                            : _themeService.getTheme().getTextColor() );
            col.setText( str );
            tr.addView( col );

        }
        return tr;
    }

    private TableRow createNewHeaderRow( final StatusGroup statusGroup )
    {
        Status status = statusGroup.getStatus( 0 );

        return createNewRow( new String[] { status.getUsernameLabel(), status.getDisplayCol1Label(),
                                 status.getDisplayCol2Label() }, Boolean.FALSE );
    }

    @SuppressWarnings( "deprecation" )
    private TableRow createNewStatusRow( final Status status )
    {
        TableRow tr = new TableRow( getApplicationContext() );
        tr.setLayoutParams( new LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT ) );
        for ( String str : new String[] { status.getUsername(), status.getDisplayCol1(), status.getDisplayCol2() } )
        {
            TextView col = new TextView( getApplicationContext() );
            col.setPadding( getDip( 5F ), getDip( 5F ), getDip( 5F ), getDip( 5F ) );
            col.setTextColor( _themeService.getTheme().getTextColor() );
            col.setText( str );
            tr.addView( col );

        }
        tr.setOnClickListener( new OnClickListener()
        {
            public void onClick( View v )
            {
                Bundle bundle = new Bundle();

                bundle.putSerializable( "status", status );

                startActivityWithFactory( REQUESTCODE_VIEWMINER, ViewMinerActivity.class, bundle );
            }
        } );
        return tr;
    }

    private class StatusGroup
    {
        public StatusMetadata statusMetadata;

        public List<Result> minerResultList;

        public List<Status> statusList;

        public void add( Status status )
        {
            if ( statusMetadata == null )
                statusMetadata = status.getMetadata();

            if ( statusList == null )
                statusList = new ArrayList<Status>();

            statusList.add( status );
        }

        public Result getMinerResult( int index )
        {
            return minerResultList.get( index );
        }

        public Status getStatus( int index )
        {
            return statusList.get( index );
        }

        public boolean hasMinerResultList()
        {
            return ( minerResultList != null ) && ( minerResultList.size() > 0 );
        }

        public boolean hasStatusList()
        {
            return ( statusList != null ) && ( statusList.size() > 0 );
        }
    }

    private void displayUserStatusUpdate()
    {
        Log.d( tag, "Status Update Start" );
        try
        {
            TableLayout mainTableLayout = (TableLayout) findViewById( R.id.statusLayout );
            mainTableLayout.removeAllViews();
            TableLayout tickerLayout = (TableLayout) findViewById( R.id.tickerLayout );
            Boolean atLeastOneExchange = Boolean.FALSE;
            for ( TickerMetadata tickerMetadata : Metadata.getTickerMetadatas( getFactory() ) )
            {
                tickerMetadata.setFactory( getFactory() );

                String key = tickerMetadata.getName();

                if ( _configuration.getShowExchange( key ) )
                {
                    try
                    {
                        List<Result> result = _minerService.readJsonData( key );
                        Ticker exchange = tickerMetadata.getDeserializer().deserialize( result.get( 0 ).getData() );
                        if ( !atLeastOneExchange )
                        {
                            atLeastOneExchange = Boolean.TRUE;
                            tickerLayout.removeAllViews();
                            tickerLayout.addView( createNewRow( new String[] { "Tickers:" }, Boolean.TRUE ) );
                            tickerLayout.addView( createNewRow( new String[] { "", "Last", "High", "Low", "Buy", "Sell" },
                                                                Boolean.FALSE ) );
                        }
                        tickerLayout.addView( createNewRow( new String[] { tickerMetadata.getLabel(),
                                                                exchange.getLastString(), exchange.getHighString(),
                                                                exchange.getLowString(), exchange.getBuyString(),
                                                                exchange.getSellString() }, Boolean.FALSE ) );

                    }
                    catch ( Exception e )
                    {
                        tickerLayout.addView( createNewRow( new String[] { tickerMetadata.getLabel() + ":", "Unable",
                            "to", "connect", "..." }, Boolean.FALSE ) );
                    }
                }
            }
            if ( !atLeastOneExchange )
            {
                tickerLayout.removeAllViews();
                tickerLayout.setVisibility( TableLayout.INVISIBLE );
            }

            boolean hashrateFormatCommas = _configuration.getHashrateFormatCommas();
            int hashrateFormatDecimals = _configuration.getHashrateFormatDecimals();
            HashrateUnit hashrateUnit = _configuration.getHashrateUnit();

            List<StatusGroup> statusGroups = new ArrayList<StatusGroup>();
            List<BigDecimal> collectedHashratesForAutomaticUnitSelection = null;

            if ( hashrateUnit.getIsAuto() )
            {
                collectedHashratesForAutomaticUnitSelection = new ArrayList<BigDecimal>();
            }

            Cursor poolCursor = _minerService.getPools();
            while ( poolCursor.moveToNext() )
            {
                String pool = poolCursor.getString( 0 );
                Cursor cursor = _minerService.getMiners( poolCursor.getString( 0 ) );

                StatusGroup statusGroup = new StatusGroup();

                statusGroup.statusMetadata = Metadata.getPoolMetadata( getFactory(), pool );

                Deserializer<? extends Status> statusDeserializer = statusGroup.statusMetadata.getDeserializer();

                while ( cursor.moveToNext() )
                {
                    int errors = cursor.getInt( 1 );
                    String apiKey = cursor.getString( 0 );

                    statusGroup.minerResultList = _minerService.readJsonData( apiKey );
                    Status status = null;
                    try
                    {
                        if ( !statusGroup.hasMinerResultList() || statusGroup.getMinerResult( 0 ).getData().equals( "" ) )
                        {
                            throw new Exception( "No JSON Data" );
                        }

                        for ( Result minerResult : statusGroup.minerResultList )
                        {
                            Status deserialized = statusDeserializer.deserialize( minerResult.getData() );

                            if ( status == null )
                                status = deserialized;
                            else
                                status.mergeWith( deserialized );
                        }

                        status.setApiKey( apiKey );

                        status.setHashrateFormat( hashrateFormatCommas, hashrateFormatDecimals, hashrateUnit );

                        if ( collectedHashratesForAutomaticUnitSelection != null )
                        {
                            status.collectHashrates( collectedHashratesForAutomaticUnitSelection );
                        }
                    }
                    catch ( Exception e )
                    {
                        _minerService.updateErrorCount( apiKey, ( errors + 1 ) );
                        int maxErrors = _configuration.getMaximumErrors();
                        Toast.makeText( getApplicationContext(),
                                        "Miner (" + apiKey + ") does not exist for pool "
                                            + statusGroup.statusMetadata.getLabel()
                                            + ", or there was no response from the server.", Toast.LENGTH_LONG ).show();
                        if ( errors >= maxErrors && maxErrors != 0 )
                        {
                            _minerService.deleteMiner( apiKey );

                            Toast.makeText( getApplicationContext(),
                                            "Max error count hit (" + maxErrors + ").  Miner removed: " + apiKey,
                                            Toast.LENGTH_LONG ).show();
                        }
                        continue;
                    }

                    // reset errors after a successful fetch
                    _minerService.updateErrorCount( apiKey, 0 );

                    statusGroup.add( status );
                }

                if ( statusGroup.hasStatusList() )
                {
                    statusGroups.add( statusGroup );
                }

                if ( ( cursor != null ) && !cursor.isClosed() )
                {
                    cursor.close();
                }
            }

            if ( poolCursor != null && !poolCursor.isClosed() )
            {
                poolCursor.close();
            }

            if ( hashrateUnit.getIsAuto() )
            {
                hashrateUnit.autoSelectBestUnit( collectedHashratesForAutomaticUnitSelection );
            }

            for ( StatusGroup statusGroup : statusGroups )
            {
                mainTableLayout.addView( createNewRow( new String[] { statusGroup.statusMetadata.getLabel() + ":" },
                                                       Boolean.TRUE ) );
                mainTableLayout.addView( createNewHeaderRow( statusGroup ) );

                for ( Status status : statusGroup.statusList )
                {
                    mainTableLayout.addView( createNewStatusRow( status ) );
                }

                if ( statusGroup.hasMinerResultList() )
                {
                    mainTableLayout.addView( createNewRow( new String[] { DateFormat.getTimeInstance( DateFormat.MEDIUM ).format( statusGroup.getMinerResult( 0 ).getDate() ) },
                                                           Boolean.TRUE ) );
                    mainTableLayout.addView( createNewRow( new String[] { "" }, Boolean.FALSE ) );
                }
            }

            Long lastUpdated = _configuration.getLastUpdated();

            if ( lastUpdated != null )
            {
                this.setTitle( "Miner Status - "
                    + DateFormat.getDateTimeInstance( DateFormat.MEDIUM, DateFormat.SHORT ).format( new Date(
                                                                                                              _configuration.getLastUpdated() ) ) );
            }
            else
            {
                this.setTitle( "Miner Status" );
            }

        }
        catch ( final RuntimeException e )
        {
            AlertDialog.Builder alert = new AlertDialog.Builder( MainMinerActivity.this );
            alert.setTitle( "MinerStatus broke something!" );
            alert.setPositiveButton( "Ignore & Continue", new DialogInterface.OnClickListener()
            {
                public void onClick( DialogInterface dialog, int whichButton )
                {
                    Toast.makeText( getApplicationContext(),
                                    "I ate the error for you (YUM).  If you would like to help debug MinerStatus, throw the Exception once and make sure you report it.  Thanks!",
                                    Toast.LENGTH_LONG ).show();
                }
            } );
            alert.setNegativeButton( "Throw Exception", new DialogInterface.OnClickListener()
            {
                public void onClick( DialogInterface dialog, int whichButton )
                {
                    throw e;
                }
            } );
            alert.show();
        }
    }

    private class MyAsynchMinerUpdateTask
        extends AsynchMinerUpdateTask
    {
        public MyAsynchMinerUpdateTask( ServiceFactory factory )
        {
            super( factory );
        }

        @Override
        protected void onPostExecute( Boolean result )
        {
            displayUserStatusUpdate();
        }
    }
}
