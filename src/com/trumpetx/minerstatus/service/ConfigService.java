package com.trumpetx.minerstatus.service;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.trumpetx.minerstatus.util.FactoryBased;
import com.trumpetx.minerstatus.util.ServiceFactory;

public class ConfigService
    implements FactoryBased
{
    private ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        return _factory;
    }

    public ConfigService( ServiceFactory factory )
    {
        this._factory = factory;
    }

    private final String SELECT_CONFIG_VALUE = "select value from config where key=?";

    public String getConfigValue( String key )
    {
        Cursor cursor = null;
        try
        {
            cursor = getDBr().rawQuery( SELECT_CONFIG_VALUE, new String[] { key } );
            if ( cursor.moveToNext() )
            {
                return cursor.getString( 0 );
            }
            else
            {
                return null;
            }
        }
        catch ( Exception e )
        {
            Log.e( "cfgService", "", e );
            return null;
        }
        finally
        {
            if ( cursor != null )
            {
                cursor.close();
            }
        }
    }

    public void setConfigValue( String key, String value )
    {
        deleteConfigValue( key );
        ContentValues values = new ContentValues();
        values.put( "key", key );
        values.put( "value", value );
        getDBw().insert( "config", null, values );
    }

    public void deleteConfigValue( String key )
    {
        getDBw().delete( "config", "key=?", new String[] { key } );
    }

    private SQLiteDatabase getDBw()
    {
        return _factory.getDbOpenHelper().getWritableDatabase();
    }

    private SQLiteDatabase getDBr()
    {
        return _factory.getDbOpenHelper().getReadableDatabase();
    }

}
