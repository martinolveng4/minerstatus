package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;

import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class EligiusStatus
    extends StatusBase
    implements Serializable, Renderable
{
    private static final long serialVersionUID = -8836018055106356450L;

    public StatusMetadata getMetadataImpl()
    {
        return new EligiusStatusMetadata();
    }

    static class EligiusStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_ELIGIUS;
        }

        @Override
        public String getLabel()
        {
            return "Eligius";
        }

        @Override
        public String getDirections()
        {
            return "Simply enter the BTC address you use to connect to the Eligius pool.";
        }
        
        @Override
        public String getAPIKeyLabel()
        {
            return "BTC Address";
        }
        
        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new EligiusStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new EligiusStatusDeserializer();
        }
    }
    
    static class EligiusStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[]{ "http://eligius.st/~wizkid057/newstats/hashrate-json.php/%MINER%" };
        }
    }
    
    private Map<Integer, EligiusHashrateInfo> _hashrateInfo;

    private List<Integer> _intervals;

    private String _apiKey;

    public void setObjectData( Map<Integer, EligiusHashrateInfo> hashrateInfo, List<Integer> intervals )
    {
        _hashrateInfo = hashrateInfo;
        _intervals = intervals;
    }

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        for ( Entry<Integer, EligiusHashrateInfo> entry : _hashrateInfo.entrySet() )
            if ( entry.getValue() != null )
                set.add( entry.getValue().getHashrate() );
    }

    @Override
    public String getUsername()
    {
        if ( _apiKey != null )
        {
            if ( _apiKey.length() > 7 )
                return _apiKey.substring( 0, 7 ) + "...";
            else
                return _apiKey;
        }
        else
            return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        return getShares().toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Shares (" + getSharesIntervalName() + ")";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getHashrate();
    }

    private EligiusHashrateInfo getPrimaryHashrateInfo()
    {
        if ( ( _intervals != null ) && ( _hashrateInfo != null ) )
        {
            int interval = _intervals.get( 0 );

            EligiusHashrateInfo hashrateInfo = _hashrateInfo.get( interval );

            if ( hashrateInfo != null )
                return hashrateInfo;
        }

        return null;
    }

    public Integer getShares()
    {
        EligiusHashrateInfo hashrateInfo = getPrimaryHashrateInfo();

        if ( hashrateInfo != null )
            return hashrateInfo.getShares();
        else
            return 0;
    }

    public String getSharesIntervalName()
    {
        EligiusHashrateInfo hashrateInfo = getPrimaryHashrateInfo();

        if ( hashrateInfo != null )
            return hashrateInfo.getIntervalName();
        else
            return "recent";
    }

    public BigDecimal getHashrate()
    {
        EligiusHashrateInfo hashrateInfo = getPrimaryHashrateInfo();

        if ( hashrateInfo != null )
            return hashrateInfo.getHashrate();
        else
            return BigDecimal.ZERO;
    }

    public String getApiKey()
    {
        return _apiKey;
    }

    public void setApiKey( String apiKey )
    {
        _apiKey = apiKey;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );

        if ( _intervals != null )
        {
            for ( int interval : _intervals )
            {
                EligiusHashrateInfo hashrateInfo = _hashrateInfo.get( interval );

                if ( hashrateInfo != null )
                {
                    tl.addView( activity.renderRow( "Interval Name", hashrateInfo.getIntervalName() ) );
                    tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( hashrateInfo.getHashrate() ) ) );
                    tl.addView( activity.renderRow( "Shares", Integer.valueOf( hashrateInfo.getShares() ).toString() ) );
                    tl.addView( activity.renderRow( "", "" ) );
                }
            }
        }
    }
}
