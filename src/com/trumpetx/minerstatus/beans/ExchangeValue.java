package com.trumpetx.minerstatus.beans;


import java.io.Serializable;
import java.math.BigDecimal;

public class ExchangeValue
    implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -306584545466972592L;

    private BigDecimal value;

    private String value_int;

    private String display;

    private String display_short;

    private String currency;

    /**
     * "last":{
     * "value":"192.00001",
     * "value_int":"19200001",
     * "display":"$192.00",
     * "display_short":"$192.00",
     * "currency":"USD"
     * }
     */
    public BigDecimal getValue()
    {
        return value;
    }

    public void setValue( BigDecimal value )
    {
        this.value = value;
    }

    public String getValue_int()
    {
        return value_int;
    }

    public void setValue_int( String value_int )
    {
        this.value_int = value_int;
    }

    public String getDisplay()
    {
        return display;
    }

    public void setDisplay( String display )
    {
        this.display = display;
    }

    public String getDisplay_short()
    {
        return display_short;
    }

    public void setDisplay_short( String display_short )
    {
        this.display_short = display_short;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency( String currency )
    {
        this.currency = currency;
    }
}
