package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BitMinterWorker
    implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 938056881666751086L;

    private boolean alive;

    private BigDecimal hash_rate;

    private Integer last_work;

    private String name;

    private Map<String, BitMinterWork> work;

    public boolean getAlive()
    {
        return this.alive;
    }

    public void setAlive( boolean alive )
    {
        this.alive = alive;
    }

    final static BigDecimal ONE_MILLION = new BigDecimal( "1000000" );

    public BigDecimal getHash_rate()
    {
        BigDecimal hash_rate_value = ( this.hash_rate == null ) ? BigDecimal.ZERO : this.hash_rate;

        return hash_rate_value.multiply( ONE_MILLION ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setHash_rate( BigDecimal hash_rate )
    {
        this.hash_rate = hash_rate.divide( ONE_MILLION );
    }

    public Integer getLast_work()
    {
        return this.last_work == null ? 0 : last_work;
    }

    public void setLast_work( Integer last_work )
    {
        this.last_work = last_work;
    }

    public String getName()
    {
        return this.name == null ? "" : name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Map<String, BitMinterWork> getWork()
    {
        return this.work == null ? new HashMap<String, BitMinterWork>( 0 ) : work;
    }

    public void setWork( Map<String, BitMinterWork> work )
    {
        this.work = work;
    }
}
