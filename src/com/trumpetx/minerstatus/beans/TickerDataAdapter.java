package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;
import com.trumpetx.minerstatus.util.FactoryBased;

public interface TickerDataAdapter
    extends FactoryBased
{
    public String getURL();

    public String fetchData( DataAgent agent );
}
