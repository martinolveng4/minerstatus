package com.trumpetx.minerstatus.beans;


import java.io.Serializable;
import java.math.BigDecimal;

public class HHTTHashrate
    implements Serializable
{

    /*
          "hashrate":{
        "last_month":55171.241,
        "last_week":62329.293,
        "last_day":66807.421,
        "last_hour":67268.732,
        "last_5_min":71468.256,
        "query_time":0.033941984176636
      }
     */

    /**
     * 
     */
    private static final long serialVersionUID = 6007177534593785706L;

    private BigDecimal last_month;

    private BigDecimal last_week;

    private BigDecimal last_day;

    private BigDecimal last_hour;

    private BigDecimal last_5_min;

    public BigDecimal getLast_month()
    {
        return ( last_month == null ? BigDecimal.ZERO : last_month ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setLast_month( BigDecimal last_month )
    {
        this.last_month = last_month;
    }

    public BigDecimal getLast_week()
    {
        return ( last_week == null ? BigDecimal.ZERO : last_week ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setLast_week( BigDecimal last_week )
    {
        this.last_week = last_week;
    }

    public BigDecimal getLast_day()
    {
        return ( last_day == null ? BigDecimal.ZERO : last_day ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setLast_day( BigDecimal last_day )
    {
        this.last_day = last_day;
    }

    public BigDecimal getLast_hour()
    {
        return ( last_hour == null ? BigDecimal.ZERO : last_hour ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setLast_hour( BigDecimal last_hour )
    {
        this.last_hour = last_hour;
    }

    public BigDecimal getLast_5_min()
    {
        return ( last_5_min == null ? BigDecimal.ZERO : last_5_min ).setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setLast_5_min( BigDecimal last_5_min )
    {
        this.last_5_min = last_5_min;
    }
}
