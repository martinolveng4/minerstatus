package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class BitclockersStatus
    extends StatusBase
    implements Serializable, Renderable
{

    /**
     *
     */
    private static final long serialVersionUID = 521022577604612057L;

    public StatusMetadata getMetadataImpl()
    {
        return new BitclockersStatusMetadata();
    }

    static class BitclockersStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_BITCLOCKERS;
        }

        @Override
        public String getLabel()
        {
            return "Bitclockers";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "on your dashboard page at\nhttp://bitclockers.com/?action=dashboard";

            return getCommonDirections( "Bitclockers", youCanGetYourAPIKey );
        }

        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new BitclockersStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new BitclockersStatusDeserializer();
        }
    }

    static class BitclockersStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "http://bitclockers.com/?action=api&cmd=%MINER%" };
        }
    }
    
    static class BitclockersStatusDeserializer
        extends GsonDeserializer<BitclockersStatus>
    {
        public BitclockersStatusDeserializer()
        {
            super(BitclockersStatus.class);
        }
    }

    private BigDecimal balance;

    private String payout;

    private BigDecimal hashrate;

    private Map<String, BitclockersWorker> workers;

    private String apiKey;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getHashrate() );

        for ( String key : getWorkers().keySet() )
        {
            BitclockersWorker worker = getWorkers().get( key );

            set.add( super.parseHashrate( worker.getHashrate() ) );
        }
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getHashrate().setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        return getBalance().toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Balance";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    public BigDecimal getBalance()
    {
        return balance == null ? BigDecimal.ZERO : balance;
    }

    public void setBalance( BigDecimal balance )
    {
        this.balance = balance;
    }

    public String getPayout()
    {
        return payout == null ? "" : payout;
    }

    public void setPayout( String payout )
    {
        this.payout = payout;
    }

    public BigDecimal getHashrate()
    {
        return hashrate == null ? BigDecimal.ZERO : hashrate;
    }

    public void setHashrate( BigDecimal hashrate )
    {
        this.hashrate = hashrate;
    }

    public Map<String, BitclockersWorker> getWorkers()
    {
        return workers;
    }

    public void setWorkers( Map<String, BitclockersWorker> workers )
    {
        this.workers = workers;
    }

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Balance", getBalance().toString() ) );
        tl.addView( activity.renderRow( "Total Withdrawn", getPayout() ) );
        tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( getHashrate() ) ) );
        if ( getWorkers() != null )
        {
            for ( String key : getWorkers().keySet() )
            {
                BitclockersWorker worker = getWorkers().get( key );
                tl.addView( activity.renderRow( "", key ) );
                tl.addView( activity.renderRow( "Shares", worker.getShares().toString() ) );
                tl.addView( activity.renderRow( "Stale", worker.getStale().toString() ) );
                tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( worker.getHashrate() ) ) );
                tl.addView( activity.renderRow( "", "" ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }
}
