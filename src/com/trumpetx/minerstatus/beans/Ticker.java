package com.trumpetx.minerstatus.beans;

public interface Ticker
    extends Plugin
{
    public TickerMetadata getMetadata();

    public String getLastString();

    public String getHighString();

    public String getLowString();

    public String getBuyString();

    public String getSellString();

    public String getVolString();

}
