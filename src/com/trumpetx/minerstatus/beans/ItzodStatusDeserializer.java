package com.trumpetx.minerstatus.beans;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.trumpetx.minerstatus.util.GsonDeserializer;

public class ItzodStatusDeserializer
    extends GsonDeserializer<ItzodStatus>
    implements JsonDeserializer<ItzodStatus>
{
    public ItzodStatusDeserializer()
    {
        super( ItzodStatus.class );
    }
    
    @Override
    public void performStartupInitialization()
    {
        GsonDeserializer.registerDeserializer( ItzodStatus.class, this );
    }

    @Override
    public ItzodStatus deserialize( JsonElement json, Type typeOfT, JsonDeserializationContext context )
        throws JsonParseException
    {
        ItzodStatus ret = new ItzodStatus();

        List<ItzodUser> users = new ArrayList<ItzodUser>();

        JsonArray array = json.getAsJsonArray();

        for ( int i = 0; i < array.size(); i++ )
        {
            try
            {
                users.add( (ItzodUser) context.deserialize( array.get( i ), ItzodUser.class ) );
            }
            catch ( Throwable t )
            {
            }
        }
        
        ItzodUser[] userArray = new ItzodUser[users.size()];

        ret.setUsers( users.toArray(userArray) );

        return ret;
    }
}
