package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.FactoryBased;

public interface Plugin
    extends FactoryBased
{
    public PluginMetadata getMetadata();
}
