package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.FactoryBased;

public interface PluginMetadata
    extends FactoryBased
{
    public String getName();
    
    public String getLabel();

    public Deserializer<? extends Plugin> getDeserializer();
}
