package com.trumpetx.minerstatus.beans;

import java.io.Serializable;

public class BitMinterWork
    implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -7387239948918772398L;

    private Integer checkpoint_accepted;

    private Integer checkpoint_rejected;

    private Integer round_accepted;

    private Integer round_rejected;

    private Integer total_accepted;

    private Integer total_rejected;

    public Integer getCheckpoint_accepted()
    {
        return this.checkpoint_accepted;
    }

    public void setCheckpoint_accepted( Integer checkpoint_accepted )
    {
        this.checkpoint_accepted = checkpoint_accepted;
    }

    public Integer getCheckpoint_rejected()
    {
        return this.checkpoint_rejected;
    }

    public void setCheckpoint_rejected( Integer checkpoint_rejected )
    {
        this.checkpoint_rejected = checkpoint_rejected;
    }

    public Integer getRound_accepted()
    {
        return this.round_accepted;
    }

    public void setRound_accepted( Integer round_accepted )
    {
        this.round_accepted = round_accepted;
    }

    public Integer getRound_rejected()
    {
        return this.round_rejected;
    }

    public void setRound_rejected( Integer round_rejected )
    {
        this.round_rejected = round_rejected;
    }

    public Integer getTotal_accepted()
    {
        return this.total_accepted;
    }

    public void setTotal_accepted( Integer total_accepted )
    {
        this.total_accepted = total_accepted;
    }

    public Integer getTotal_rejected()
    {
        return this.total_rejected;
    }

    public void setTotal_rejected( Integer total_rejected )
    {
        this.total_rejected = total_rejected;
    }
}
