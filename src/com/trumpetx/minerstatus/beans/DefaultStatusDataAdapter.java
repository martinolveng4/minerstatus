package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;
import com.trumpetx.minerstatus.util.ServiceFactory;

public abstract class DefaultStatusDataAdapter
    implements StatusDataAdapter
{
    ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        if ( _factory != null )
            return _factory;
        else
            return ServiceFactory.getDefaultInstance();
    }

    public String[] getURLs( String apiKey )
    {
        String[] urlTemplates = getURLTemplates();

        for ( int i = 0; i < urlTemplates.length; i++ )
            urlTemplates[i] = urlTemplates[i].replace( "%MINER%", apiKey );

        return urlTemplates;
    }

    public String[] fetchData( String apiKey, DataAgent agent )
    {
        String[] urls = getURLs( apiKey );
        String[] data = new String[urls.length];

        for ( int i = 0; i < urls.length; i++ )
        {
            try
            {
                data[i] = agent.fetchData( urls[i] );
            }
            catch ( Throwable t )
            {
                data[i] = "invalid: " + t;
            }
        }

        return data;
    }
}
