package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class HHTTStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /*
    {
      "user":"1GQAFP1yGcQfa94MvjD8bwqS6XPkCWnKiy",
      "monies":{
        "Shares":520245,
        "Diff-1-Shares":33295680,
        "Earned":120.63232292,
        "Paid":120.63232292,
        "Owed":0,
        "query_time":0.0044929981231689
      },
      "hashrate":{
        "last_month":55171.241,
        "last_week":62329.293,
        "last_day":66807.421,
        "last_hour":67268.732,
        "last_5_min":71468.256,
        "query_time":0.033941984176636
      }
    }
    */

    /**
     * 
     */
    private static final long serialVersionUID = -2717315304290010721L;

    public StatusMetadata getMetadataImpl()
    {
        return new HHTTStatusMetadata();
    }

    static class HHTTStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_HHTT;
        }

        @Override
        public String getLabel()
        {
            return "HHTT";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "from the main HHTT page at\nhttp://hhtt.1209k.com/";
            
            return getCommonDirections( "The Horrible Horrendous Terrible Tremendous Mining Pool", youCanGetYourAPIKey );
        }

        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new HHTTStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new HHTTStatusDeserializer();
        }
    }
    
    static class HHTTStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[]{ "https://hhtt.1209k.com/user-details-json.php?user=%MINER%" };
        }
    }
    
    static class HHTTStatusDeserializer
        extends GsonDeserializer<HHTTStatus>
    {
        public HHTTStatusDeserializer()
        {
            super(HHTTStatus.class);
        }
    }

    private String user;

    private String apiKey;

    private HHTTHashrate hashrate;

    private HHTTMonies monies;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );

        set.add( getHashrate().getLast_5_min() );
        set.add( getHashrate().getLast_hour() );
        set.add( getHashrate().getLast_week() );
        set.add( getHashrate().getLast_month() );
    }

    @Override
    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "User", getUser() ) );
        tl.addView( activity.renderRow( "Shares", getMonies().getShares() ) );
        tl.addView( activity.renderRow( "Earned", getMonies().getEarned() ) );
        tl.addView( activity.renderRow( "Paid", getMonies().getPaid() ) );
        tl.addView( activity.renderRow( "Owed", getMonies().getOwed() ) );
        tl.addView( activity.renderRow( "", "" ) );
        tl.addView( activity.renderRow( "Last 5 Min", super.formatHashrate( getHashrate().getLast_5_min() ) ) );
        tl.addView( activity.renderRow( "Last Hour", super.formatHashrate( getHashrate().getLast_hour() ) ) );
        tl.addView( activity.renderRow( "Last Week", super.formatHashrate( getHashrate().getLast_week() ) ) );
        tl.addView( activity.renderRow( "Last Month", super.formatHashrate( getHashrate().getLast_month() ) ) );
        tl.addView( activity.renderRow( "", "" ) );
    }

    @Override
    public String getUsername()
    {
        return getUser().length() > 8 ? getUser().substring( 0, 5 ) + "..." : getUser();
    }

    @Override
    public String getDisplayCol1()
    {
        return getMonies().getPaid().toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return CONFIRMED_REWARD_COL_1_LABEL;
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getHashrate().getLast_5_min();
    }

    @Override
    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public String getApiKey()
    {
        return apiKey;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser( String user )
    {
        this.user = user;
    }

    public HHTTHashrate getHashrate()
    {
        return hashrate == null ? new HHTTHashrate() : hashrate;
    }

    public void setHashrate( HHTTHashrate hashrate )
    {
        this.hashrate = hashrate;
    }

    public HHTTMonies getMonies()
    {
        return monies == null ? new HHTTMonies() : monies;
    }

    public void setMonies( HHTTMonies monies )
    {
        this.monies = monies;
    }
}
