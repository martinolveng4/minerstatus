package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class BtcMineStatus
    extends StatusBase
    implements Serializable, Renderable
{

    /**
     *
     */
    private static final long serialVersionUID = 4929377426083392967L;

    public StatusMetadata getMetadataImpl()
    {
        return new BtcMineStatusMetadata();
    }

    static class BtcMineStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_BTCMINE;
        }

        @Override
        public String getLabel()
        {
            return "BtcMine";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "(and generate new ones) on your profile page at\nhttp://btcmine.com/user/profile/";

            return getCommonDirections( "BtcMine", youCanGetYourAPIKey );
        }

        @Override
        public StatusDataAdapter getDataAdapterImpl()
        {
            return new BtcMineStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializerImpl()
        {
            return new BtcMineStatusDeserializer();
        }
    }
    
    static class BtcMineStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return
                new String[]
                {
                    "http://btcmine.com/api/getstats/%MINER%/",
                    "http://btcmine.com/api/getminerstats/%MINER%/"
                };
        }
    }
    
    static class BtcMineStatusDeserializer
        extends GsonDeserializer<BtcMineStatus>
    {
        public BtcMineStatusDeserializer()
        {
            super(BtcMineStatus.class);
        }
    }

    private String total_bounty;

    private String confirmed_bounty;

    private Integer solved_blocks;

    private Integer round_shares;

    private String estimated_bounty;

    private Integer solved_shares;

    private String unconfirmed_bounty;

    private String hashrate;

    private String total_payout;

    private BtcMineWorker[] miners;

    private String apiKey;

    private Boolean online_status;

    private String total_24h;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        return super.parseHashrate( hashrate );
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getDisplayCol1()
    {
        return ( total_payout == null ) ? "" : total_payout;
    }

    @Override
    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public String getApiKey()
    {
        return apiKey;
    }

    public String getTotal_bounty()
    {
        return ( total_bounty == null ) ? "" : total_bounty;
    }

    public void setTotal_bounty( String total_bounty )
    {
        this.total_bounty = total_bounty;
    }

    public String getConfirmed_bounty()
    {
        return ( confirmed_bounty == null ) ? "" : confirmed_bounty;
    }

    public void setConfirmed_bounty( String confirmed_bounty )
    {
        this.confirmed_bounty = confirmed_bounty;
    }

    public Integer getSolved_blocks()
    {
        return ( solved_blocks == null ) ? 0 : solved_blocks;
    }

    public void setSolved_blocks( Integer solved_blocks )
    {
        this.solved_blocks = solved_blocks;
    }

    public Integer getRound_shares()
    {
        return ( round_shares == null ) ? 0 : round_shares;
    }

    public void setRound_shares( Integer round_shares )
    {
        this.round_shares = round_shares;
    }

    public String getEstimated_bounty()
    {
        return ( estimated_bounty == null ) ? "" : estimated_bounty;
    }

    public void setEstimated_bounty( String estimated_bounty )
    {
        this.estimated_bounty = estimated_bounty;
    }

    public Integer getSolved_shares()
    {
        return ( solved_shares == null ) ? 0 : solved_shares;
    }

    public void setSolved_shares( Integer solved_shares )
    {
        this.solved_shares = solved_shares;
    }

    public String getUnconfirmed_bounty()
    {
        return ( unconfirmed_bounty == null ) ? "" : unconfirmed_bounty;
    }

    public void setUnconfirmed_bounty( String unconfirmed_bounty )
    {
        this.unconfirmed_bounty = unconfirmed_bounty;
    }

    public String getHashrate()
    {
        return ( hashrate == null ) ? "" : hashrate;
    }

    public void setHashrate( String hashrate )
    {
        this.hashrate = hashrate;
    }

    public String getTotal_payout()
    {
        return ( total_payout == null ) ? "" : total_payout;
    }

    public void setTotal_payout( String total_payout )
    {
        this.total_payout = total_payout;
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Total Payout";
    }

    public BtcMineWorker[] getMiners()
    {
        return miners;
    }

    public void setMiners( BtcMineWorker[] miners )
    {
        this.miners = miners;
    }

    @Override
    public BtcMineStatus mergeWith( Status object )
    {
        if ( object instanceof BtcMineStatus )
        {
            BtcMineStatus btcMine = (BtcMineStatus) object;
            if ( btcMine.total_bounty != null )
            {
                this.total_bounty = btcMine.total_bounty;
            }
            if ( btcMine.confirmed_bounty != null )
            {
                this.confirmed_bounty = btcMine.confirmed_bounty;
            }
            if ( btcMine.solved_blocks != null )
            {
                this.solved_blocks = btcMine.solved_blocks;
            }
            if ( btcMine.round_shares != null )
            {
                this.round_shares = btcMine.round_shares;
            }
            if ( btcMine.estimated_bounty != null )
            {
                this.estimated_bounty = btcMine.estimated_bounty;
            }
            if ( btcMine.solved_shares != null )
            {
                this.solved_shares = btcMine.solved_shares;
            }
            if ( btcMine.unconfirmed_bounty != null )
            {
                this.unconfirmed_bounty = btcMine.unconfirmed_bounty;
            }
            if ( btcMine.hashrate != null )
            {
                this.hashrate = btcMine.hashrate;
            }
            if ( btcMine.total_payout != null )
            {
                this.total_payout = btcMine.total_payout;
            }
            if ( btcMine.miners != null )
            {
                this.miners = btcMine.miners;
            }
            if ( btcMine.online_status != null )
            {
                this.online_status = btcMine.online_status;
            }
            if ( btcMine.total_24h != null )
            {
                this.total_24h = btcMine.total_24h;
            }
            return this;
        }
        else
        {
            throw new RuntimeException( "Cannot merge these two objects." );
        }
    }

    public Boolean getOnline_status()
    {
        return ( online_status == null ) ? Boolean.FALSE : online_status;
    }

    public void setOnline_status( Boolean online_status )
    {
        this.online_status = online_status;
    }

    public String getTotal_24h()
    {
        return ( total_24h == null ) ? "" : total_24h;
    }

    public void setTotal_24h( String total_24h )
    {
        this.total_24h = total_24h;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( getHashrate() ) ) );
        tl.addView( activity.renderRow( "Total Payout", getTotal_payout() ) );
        tl.addView( activity.renderRow( "Total Bounty", getTotal_bounty() ) );
        tl.addView( activity.renderRow( "Confirmed Bounty", getConfirmed_bounty() ) );
        tl.addView( activity.renderRow( "Estimated Bounty", getEstimated_bounty() ) );
        tl.addView( activity.renderRow( "Unconfirmed Bounty", getUnconfirmed_bounty() ) );
        tl.addView( activity.renderRow( "Round Shares", getRound_shares().toString() ) );
        tl.addView( activity.renderRow( "Solved Shares", getSolved_shares().toString() ) );
        tl.addView( activity.renderRow( "Solved Blocks", getSolved_blocks().toString() ) );
        tl.addView( activity.renderRow( "", "" ) );
        if ( getMiners() != null )
        {
            for ( BtcMineWorker worker : getMiners() )
            {
                tl.addView( activity.renderRow( "", worker.getName() ) );
                tl.addView( activity.renderRow( "Online Status", worker.getOnline_status().toString() ) );
                tl.addView( activity.renderRow( "Date Connected", worker.getDate_connected() ) );
                tl.addView( activity.renderRow( "Solved Shares", worker.getSolved_shares().toString() ) );
                tl.addView( activity.renderRow( "Solved Blocks", worker.getSolved_blocks().toString() ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }

}
