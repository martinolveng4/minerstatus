package com.trumpetx.minerstatus.beans;

import android.util.Log;

import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;
import com.trumpetx.minerstatus.util.ServiceFactory;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;

public abstract class StatusBase
    implements Status
{
    /**
     * 
     */
    private static final long serialVersionUID = -787734117653681764L;

    transient ServiceFactory _factory;

    boolean _useComma;

    int _decimalPlaces;

    HashrateUnit _unit;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        if ( _factory == null )
        {
            // Chances are we've just been deserialized, and our consumer has forgotten to propagate a
            // ServiceFactory instance to us.
            Log.d( "StatusBase", "No factory set in StatusBase's FactoryBased.getFactory implementation." );

            _factory = ServiceFactory.getDefaultInstance();
        }

        return _factory;
    }

    protected abstract StatusMetadata getMetadataImpl();

    @Override
    public final StatusMetadata getMetadata()
    {
        StatusMetadata metadata = getMetadataImpl();

        metadata.setFactory( _factory );

        return metadata;
    }

    public void setHashrateFormat( boolean useComma, int decimalPlaces, HashrateUnit unit )
    {
        _useComma = useComma;
        _decimalPlaces = decimalPlaces;
        _unit = unit;
    }

    @Override
    public Status mergeWith( Status other )
    {
        String msg = "Internal Error: The " + this.getClass().getName() + " type does not support merging results.";

        throw new UnsupportedOperationException( msg );
    }

    public String formatHashrate( BigDecimal hashrate )
    {
        HashrateUnit unit = _unit;

        if ( unit.getIsAutoForEach() )
        {
            // Detach from the main instance so that we don't interfere with anybody else.
            unit = unit.clone();

            // Reassess the best unit based on just this one hashrate.
            ArrayList<BigDecimal> thisHashrate = new ArrayList<BigDecimal>();

            thisHashrate.add( hashrate );

            unit.autoSelectBestUnit( thisHashrate );
        }

        if ( unit.getScale() != 0.0 )
        {
            try
            {
                hashrate = hashrate.divide( BigDecimal.valueOf( unit.getScale() ) );
                hashrate = hashrate.setScale( _decimalPlaces, BigDecimal.ROUND_HALF_UP );
            }
            catch ( Throwable t )
            {
            }
        }

        NumberFormat formatter = NumberFormat.getNumberInstance();

        formatter.setGroupingUsed( _useComma );
        formatter.setMinimumFractionDigits( _decimalPlaces );
        formatter.setMaximumFractionDigits( _decimalPlaces );

        return formatter.format( hashrate ) + " " + unit.getName();
    }

    public String formatHashrate( String hashrateString )
    {
        try
        {
            return formatHashrate( parseHashrate( hashrateString ) );
        }
        catch ( Exception e )
        {
            return formatHashrate( BigDecimal.valueOf( 0 ) );
        }
    }

    public static BigDecimal parseHashrate( String formattedHashrate )
    {
        // Makes a best effort to parse a variety of ways a hashrate could be formatted, such as "3 khash" or
        // "29.6 TH/s".
        StringBuffer numericalPart = new StringBuffer();
        String remainder = "";

        for ( int i = 0; i < formattedHashrate.length(); i++ )
        {
            char ch = formattedHashrate.charAt( i );

            if ( ( ch == '.' ) || Character.isDigit( ch ) )
            {
                numericalPart.append( ch );
            }
            else
            {
                remainder = formattedHashrate.substring( i );
                break;
            }
        }

        try
        {
            BigDecimal parsed = new BigDecimal( numericalPart.toString() );
            double scale = 1e0;

            for ( int i = 0; i < remainder.length(); i++ )
            {
                switch ( remainder.charAt( i ) )
                {
                    case 'k':
                    case 'K':
                        scale = 1e3;
                        break;
                    case 'm':
                    case 'M':
                        scale = 1e6;
                        break;
                    case 'g':
                    case 'G':
                        scale = 1e9;
                        break;
                    case 't':
                    case 'T':
                        scale = 1e12;
                        break;

                    default:
                        continue;
                }

                break;
            }

            parsed = parsed.multiply( BigDecimal.valueOf( scale ) );

            return parsed;
        }
        catch ( Exception ex )
        {
            return BigDecimal.ZERO;
        }
    }
}
