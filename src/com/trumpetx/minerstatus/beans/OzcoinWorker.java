package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class OzcoinWorker
    implements Serializable
{

    /* @formatter:off */
    /*
    {
      "comment":"",
      "current_speed":"3,911.00",
      "valid_shares":"15981",
      "invalid_shares":"22",
      "efficiency":"99.86",
      "idle_since":"0000-00-00 00:00:00",
      "idle_since_unix":"0",
      "valid_shares_lifetime":"1100888",
      "invalid_shares_lifetime":"1960"
    }
     */
    /* @formatter:on */

    /**
     * 
     */
    private static final long serialVersionUID = -708021592795206217L;

    private String current_speed;

    private BigDecimal efficiency;

    private String idle_since;

    private BigInteger invalid_shares;

    private BigInteger invalid_shares_lifetime;

    private BigInteger valid_shares;

    private BigInteger valid_shares_lifetime;

    public BigDecimal getCurrent_speed()
    {
        try
        {
            if ( current_speed != null )
            {
                String current_speed_parseable = current_speed.replace( ",", "" );

                return new BigDecimal( current_speed_parseable ).setScale( 2, BigDecimal.ROUND_HALF_UP );
            }
        }
        catch ( Throwable t )
        {
        }

        return BigDecimal.ZERO;
    }

    public void setCurrent_speed( BigDecimal current_speed )
    {
        this.current_speed = current_speed.toString();
    }

    public BigDecimal getEfficiency()
    {
        return efficiency == null ? new BigDecimal( 100 ) : efficiency.setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public void setEfficiency( BigDecimal efficiency )
    {
        this.efficiency = efficiency;
    }

    public String getIdle_since()
    {
        return idle_since == null ? "" : idle_since;
    }

    public void setIdle_since( String idle_since )
    {
        this.idle_since = idle_since;
    }

    public BigInteger getInvalid_shares()
    {
        return invalid_shares == null ? BigInteger.ZERO : invalid_shares;
    }

    public void setInvalid_shares( BigInteger invalid_shares )
    {
        this.invalid_shares = invalid_shares;
    }

    public BigInteger getInvalid_shares_lifetime()
    {
        return invalid_shares_lifetime == null ? BigInteger.ZERO : invalid_shares_lifetime;
    }

    public void setInvalid_shares_lifetime( BigInteger invalid_shares_lifetime )
    {
        this.invalid_shares_lifetime = invalid_shares_lifetime;
    }

    public BigInteger getValid_shares()
    {
        return valid_shares == null ? BigInteger.ZERO : valid_shares;
    }

    public void setValid_shares( BigInteger valid_shares )
    {
        this.valid_shares = valid_shares;
    }

    public BigInteger getValid_shares_lifetime()
    {
        return valid_shares_lifetime == null ? BigInteger.ZERO : valid_shares_lifetime;
    }

    public void setValid_shares_lifetime( BigInteger valid_shares_lifetime )
    {
        this.valid_shares_lifetime = valid_shares_lifetime;
    }
}
