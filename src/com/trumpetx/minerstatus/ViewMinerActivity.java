package com.trumpetx.minerstatus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.trumpetx.minerstatus.beans.Renderable;
import com.trumpetx.minerstatus.beans.Status;
import com.trumpetx.minerstatus.service.ThemeService;

public class ViewMinerActivity
    extends AbstractMinerStatusActivity
{
    ThemeService _themeService;
    
    private Status minerStatus;

    @Override
    protected void onCreateImpl( Bundle savedInstanceState )
    {
        _themeService = getFactory().getThemeService();
        
        Bundle bundle = this.getIntent().getExtras();
        
        minerStatus = (Status) bundle.getSerializable( "status" );
        
        setContentView( R.layout.viewminer );
        int bgColor = _themeService.getTheme().getBackgroundColor();
        ScrollView scrollView = (ScrollView) findViewById( R.id.viewMinerScrollView );
        scrollView.setBackgroundColor( bgColor );

        try
        {
            populateDetailedView();
        }
        catch ( final NullPointerException e )
        {
            AlertDialog.Builder alert = new AlertDialog.Builder( ViewMinerActivity.this );
            alert.setTitle( "MinerStatus broke something!" );
            alert.setPositiveButton( "Ignore & Continue", new DialogInterface.OnClickListener()
            {
                public void onClick( DialogInterface dialog, int whichButton )
                {
                    Toast.makeText( getApplicationContext(),
                                    "I ate the error for you (YUM).  If you would like to help debug MinerStatus, throw the Exception once and make sure you report it.  Thanks!",
                                    Toast.LENGTH_LONG ).show();
                }
            } );
            alert.setNegativeButton( "Throw Exception", new DialogInterface.OnClickListener()
            {
                public void onClick( DialogInterface dialog, int whichButton )
                {
                    throw e;
                }
            } );
            alert.show();
        }

        Button deleteMinerButton = (Button) findViewById( R.id.deleteMinerButton );
        deleteMinerButton.setOnClickListener( new OnClickListener()
        {
            public void onClick( View v )
            {
                AlertDialog.Builder alert = new AlertDialog.Builder( ViewMinerActivity.this );
                alert.setTitle( minerStatus.getApiKey() );
                alert.setPositiveButton( "Remove", new DialogInterface.OnClickListener()
                {
                    public void onClick( DialogInterface dialog, int whichButton )
                    {
                        Toast.makeText( getApplicationContext(), minerStatus.getApiKey() + " removed.",
                                        Toast.LENGTH_LONG ).show();
                        getFactory().getMinerService().deleteMiner( minerStatus.getApiKey() );
                        ViewMinerActivity.this.finish();
                    }
                } );
                alert.setNegativeButton( "Cancel", new DialogInterface.OnClickListener()
                {
                    public void onClick( DialogInterface dialog, int whichButton )
                    {
                        dialog.cancel();
                    }
                } );
                alert.show();
            }
        } );

    }

    private void populateDetailedView()
    {

        if ( minerStatus instanceof Renderable )
        {
            Renderable renderableMinerStatus = (Renderable) minerStatus;
            renderableMinerStatus.render( this );
        }
        else
        {
            TableLayout tl = (TableLayout) findViewById( R.id.detailedView );
            tl.setVisibility( TableLayout.INVISIBLE );
        }

    }

    public TableRow renderRow( String left, Object right )
    {
        TableRow tr = new TableRow( this );
        TextView leftCol = new TextView( getApplicationContext() );
        leftCol.setPadding( getDip( 5F ), getDip( 5F ), getDip( 5F ), getDip( 5F ) );
        leftCol.setTextColor( _themeService.getTheme().getHeaderTextColor() );
        leftCol.setText( left );
        tr.addView( leftCol );
        TextView rightCol = new TextView( getApplicationContext() );
        rightCol.setPadding( getDip( 10F ), getDip( 5F ), getDip( 5F ), getDip( 5F ) );
        rightCol.setTextColor( _themeService.getTheme().getTextColor() );
        rightCol.setText( right.toString() );
        tr.addView( rightCol );
        return tr;
    }

}
