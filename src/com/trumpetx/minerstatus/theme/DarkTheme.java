package com.trumpetx.minerstatus.theme;

import android.graphics.Color;

public class DarkTheme
    implements Theme
{

    @Override
    public String getName()
    {
        return "dark";
    }

    @Override
    public int getTextColor()
    {
        return Color.WHITE;
    }

    @Override
    public int getBackgroundColor()
    {
        return Color.BLACK;
    }

    @Override
    public int getHeaderTextColor()
    {
        return Color.GRAY;
    }

    @Override
    public int getWidgetCol1Color()
    {
        return 0xFFCCCCCC;
    }

    @Override
    public int getWidgetCol1LabelColor()
    {
        return 0xFFBBBBBB;
    }

    @Override
    public int getWidgetCol2Color()
    {
        return 0xFF00FF00;
    }

    @Override
    public int getWidgetCol2LowHashrateColor()
    {
        return 0xFFFF0000;
    }

    @Override
    public int getWidgetPoolColor()
    {
        return Color.WHITE;
    }

    @Override
    public int getWidgetLastUpdateColor()
    {
        // TODO Auto-generated method stub
        return 0xFFCCCCCC;
    }

}
