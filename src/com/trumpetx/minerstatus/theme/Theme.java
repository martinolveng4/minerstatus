package com.trumpetx.minerstatus.theme;

public interface Theme
{

    public String getName();

    public int getTextColor();

    public int getBackgroundColor();

    public int getHeaderTextColor();

    public int getWidgetCol1Color();

    public int getWidgetCol1LabelColor();

    public int getWidgetCol2Color();

    public int getWidgetCol2LowHashrateColor();

    public int getWidgetPoolColor();

    public int getWidgetLastUpdateColor();

}
