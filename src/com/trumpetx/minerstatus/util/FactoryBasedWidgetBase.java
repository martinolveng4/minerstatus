package com.trumpetx.minerstatus.util;

import com.trumpetx.minerstatus.util.FactoryBasedServiceBase;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public abstract class FactoryBasedWidgetBase
    extends AppWidgetProvider
    implements FactoryBased
{
    ServiceFactory _factory;

    @Override
    public void setFactory( ServiceFactory factory )
    {
        _factory = factory;
    }

    @Override
    public ServiceFactory getFactory()
    {
        return _factory;
    }

    protected abstract void onUpdateImpl( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds );
    
    @Override
    public final void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
    {
        ServiceFactory.setApplicationContext( context.getApplicationContext() );
        
        onUpdateImpl( context, appWidgetManager, appWidgetIds );
    }

    protected void startServiceWithFactory( Context context, Class<? extends FactoryBasedServiceBase> serviceType )
    {
        Intent intent = new Intent( context, serviceType );
        
        FactoryBasedUtility.injectFactory( this, intent );

        context.startService( intent );
    }
}
