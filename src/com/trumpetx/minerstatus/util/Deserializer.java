package com.trumpetx.minerstatus.util;

public interface Deserializer<T extends FactoryBased>
    extends FactoryBased
{
    void performStartupInitialization();

    T deserialize( String data );
}
