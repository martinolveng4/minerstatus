package com.trumpetx.minerstatus.hashrateunits;

import java.util.ArrayList;

public class HashrateList
{
    private HashrateUnit _selectedItem;

    private ArrayList<HashrateUnit> _listItems;

    public String getName()
    {
        return _selectedItem.getName();
    }

    public ArrayList<HashrateUnit> getItems()
    {
        return _listItems;
    }

    public void setItems( ArrayList<HashrateUnit> newValue )
    {
        _listItems = newValue;
    }

    public HashrateUnit getSelectedItem()
    {
        return _selectedItem;
    }

    public void setSelectedItem( HashrateUnit newValue )
    {
        _selectedItem = newValue;
    }

    public HashrateUnit getItemAt( int index )
    {
        return _listItems.get( index );
    }

    public boolean getItemAtSelected( int index )
    {
        HashrateUnit itemAtIndex = getItemAt( index );

        if ( itemAtIndex.getScale() < 0 )
        {
            // This is the Custom option -- mark it selected if _selectedItem is none of the other _listItems.
            for ( HashrateUnit listItem : _listItems )
            {
                if ( _selectedItem.equals( listItem ) && !itemAtIndex.equals( listItem ) )
                {
                    return false;
                }
            }

            return true;
        }
        else
        {
            // This is an actual option -- mark it selected if _selectedItem is equal to it.
            return _selectedItem.equals( itemAtIndex );
        }
    }
}
