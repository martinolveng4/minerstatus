package com.trumpetx.minerstatus.hashrateunits;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import com.trumpetx.minerstatus.R;

public class HashrateListAdapter
    extends BaseExpandableListAdapter
{
    private Context _context;

    private HashrateList _group;

    private OnGroupClickListener _onGroupClickListener;

    public HashrateListAdapter( Context context, HashrateList group )
    {
        _context = context;
        _group = group;
    }

    public void setOnGroupClickListener( OnGroupClickListener listener )
    {
        _onGroupClickListener = listener;
    }

    @Override
    public Object getChild( int groupPosition, int childPosition )
    {
        return _group.getItemAt( childPosition );
    }

    @Override
    public long getChildId( int groupPosition, int childPosition )
    {
        return childPosition;
    }

    @Override
    public View getChildView( int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup parent )
    {
        HashrateUnit listItem = _group.getItemAt( childPosition );

        if ( view == null )
        {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

            view = inflater.inflate( R.layout.hashrateunit_listitem, null );
        }

        RadioButton hashrateUnitRadioButton = (RadioButton) view.findViewById( R.id.hashrateUnitRadioButton );
        TextView hashrateUnitLabel = (TextView) view.findViewById( R.id.hashrateUnitLabel );

        hashrateUnitRadioButton.setChecked( _group.getItemAtSelected( childPosition ) );
        hashrateUnitLabel.setText( listItem.getName() );

        return view;
    }

    @Override
    public int getChildrenCount( int groupPosition )
    {
        return _group.getItems().size();
    }

    @Override
    public Object getGroup( int arg0 )
    {
        return _group;
    }

    @Override
    public int getGroupCount()
    {
        return 1;
    }

    @Override
    public long getGroupId( int groupPosition )
    {
        return groupPosition;
    }

    @Override
    public View getGroupView( final int groupPosition, boolean isLastChild, View view, ViewGroup parent )
    {
        if ( view == null )
        {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

            view = inflater.inflate( R.layout.hashrateunit_header, null );
        }

        final View viewRef = view;

        Button selectedHashrateUnitButton = (Button) view.findViewById( R.id.selectedHashrateUnitButton );

        selectedHashrateUnitButton.setText( _group.getName() );
        selectedHashrateUnitButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                if ( _onGroupClickListener != null )
                {
                    _onGroupClickListener.onGroupClicked( viewRef, groupPosition );
                }
            }
        } );

        return view;
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }

    @Override
    public boolean isChildSelectable( int groupPosition, int childPosition )
    {
        return true;
    }

    public interface OnGroupClickListener
    {
        void onGroupClicked( View view, int groupPosition );
    }
}
