package com.trumpetx.minerstatus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.trumpetx.minerstatus.beans.StatusBase;
import com.trumpetx.minerstatus.beans.TickerMetadata;
import com.trumpetx.minerstatus.currency.Currency;
import com.trumpetx.minerstatus.hashrateunits.HashrateList;
import com.trumpetx.minerstatus.hashrateunits.HashrateListAdapter;
import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;
import com.trumpetx.minerstatus.service.ThemeService;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Metadata;
import com.trumpetx.minerstatus.util.NumberPicker;
import com.trumpetx.minerstatus.util.NumberPicker.OnChangedListener;
import java.math.BigDecimal;
import java.util.ArrayList;

import static com.trumpetx.minerstatus.util.Metadata.*;

public class OptionsActivity
    extends AbstractMinerStatusActivity
{
    Configuration _configuration;

    private static final LayoutParams TOGGLE_PARAMS;

    static
    {
        TOGGLE_PARAMS = new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
        TOGGLE_PARAMS.setMargins( 20, 0, 0, 15 );
    }

    private ToggleButton generateToggleButton( final ToggleSetting setting, final String textOn, final String textOff )
    {
        final ToggleButton toggle = new ToggleButton( OptionsActivity.this );

        toggle.setLayoutParams( TOGGLE_PARAMS );
        toggle.setTextOn( "Visible" );
        toggle.setTextOff( "Hidden" );
        toggle.setChecked( setting.get() );
        toggle.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                setting.set( toggle.isChecked() );
                Toast.makeText( OptionsActivity.this, ( toggle.isChecked() ) ? textOn : textOff, Toast.LENGTH_SHORT ).show();
            }
        } );

        return toggle;
    }

    interface ToggleSetting
    {
        boolean get();

        void set( boolean newValue );
    }

    private TextView generateTextViewForToggleButton( String label )
    {
        TextView tv = new TextView( OptionsActivity.this );
        tv.setText( label );
        tv.setLayoutParams( TOGGLE_PARAMS );
        tv.setTextColor( color );

        return tv;
    }

    private int color;

    private int bgColor;

    ScrollView optionsScrollView;

    LinearLayout optionsLayout;

    TextView themeSpinnerLabel;

    RadioButton radio_dark;

    RadioButton radio_light;

    TextView connectionTimeoutLabel;

    NumberPicker connectionTimeoutPicker;

    TextView maxErrorsLabel;

    NumberPicker maxErrorsPicker;

    TextView hashrateFormatCommasLabel;

    CheckBox hashrateFormatCommasCheckBox;

    TextView hashrateFormatDecimalsLabel;

    NumberPicker hashrateFormatDecimalsPicker;

    TextView hashrateFormatUnitsLabel;

    ExpandableListView hashrateFormatUnitsList;

    TextView widgetSpinnerLabel;

    Spinner widget_spinner;

    TextView widgetThemeSpinnerLabel;

    RadioButton radio_widget_dark;

    RadioButton radio_widget_light;

    TextView lowHashrateNotificationLabel;

    EditText lowHashrateInput;

    Button lowHashrateSaveButton;

    TextView vibrateOnNotificationLabel;

    CheckBox vibrateOnNotificationCheckBox;

    TextView deleteSpinnerLabel;

    Spinner miner_delete_spinner;

    Button deleteMinerButtonOptionsMenu;

    TextView currencyLabel;

    Spinner currencyListView;

    private void findViews()
    {
        optionsScrollView = (ScrollView) findViewById( R.id.optionsScrollView );
        optionsLayout = (LinearLayout) findViewById( R.id.optionsLayout );
        themeSpinnerLabel = (TextView) findViewById( R.id.themeSpinnerLabel );
        radio_dark = (RadioButton) findViewById( R.id.radio_dark );
        radio_light = (RadioButton) findViewById( R.id.radio_light );
        connectionTimeoutLabel = (TextView) findViewById( R.id.connectionTimeoutLabel );
        connectionTimeoutPicker = (NumberPicker) findViewById( R.id.connectionTimeoutPicker );
        maxErrorsLabel = (TextView) findViewById( R.id.maxErrorsLabel );
        maxErrorsPicker = (NumberPicker) findViewById( R.id.maxErrorsPicker );
        hashrateFormatCommasLabel = (TextView) findViewById( R.id.hashrateFormatCommasLabel );
        hashrateFormatCommasCheckBox = (CheckBox) findViewById( R.id.hashrateFormatCommasCheckBox );
        hashrateFormatDecimalsLabel = (TextView) findViewById( R.id.hashrateFormatDecimalsLabel );
        hashrateFormatDecimalsPicker = (NumberPicker) findViewById( R.id.hashrateFormatDecimalsPicker );
        hashrateFormatUnitsLabel = (TextView) findViewById( R.id.hashrateFormatUnitsLabel );
        hashrateFormatUnitsList = (ExpandableListView) findViewById( R.id.hashrateFormatUnitsList );
        currencyLabel = (TextView) findViewById( R.id.currencyLabel );
        currencyListView = (Spinner) findViewById( R.id.currencyListView );
        widgetSpinnerLabel = (TextView) findViewById( R.id.widgetSpinnerLabel );
        widget_spinner = (Spinner) findViewById( R.id.widget_spinner );
        widgetThemeSpinnerLabel = (TextView) findViewById( R.id.widgetThemeSpinnerLabel );
        radio_widget_dark = (RadioButton) findViewById( R.id.radio_widget_dark );
        radio_widget_light = (RadioButton) findViewById( R.id.radio_widget_light );
        lowHashrateNotificationLabel = (TextView) findViewById( R.id.lowHashrateNotificationLabel );
        lowHashrateInput = (EditText) findViewById( R.id.lowHashrateInput );
        lowHashrateSaveButton = (Button) findViewById( R.id.lowHashrateSaveButton );
        vibrateOnNotificationLabel = (TextView) findViewById( R.id.vibrateOnNotificationLabel );
        vibrateOnNotificationCheckBox = (CheckBox) findViewById( R.id.vibrateOnNotificationCheckBox );
        deleteSpinnerLabel = (TextView) findViewById( R.id.deleteSpinnerLabel );
        miner_delete_spinner = (Spinner) findViewById( R.id.miner_delete_spinner );
        deleteMinerButtonOptionsMenu = (Button) findViewById( R.id.deleteMinerButtonOptionsMenu );
    }

    @Override
    protected void onCreateImpl( Bundle savedInstanceState )
    {
        try
        {
            _configuration = getFactory().getConfiguration();

            setContentView( R.layout.options );

            findViews();

            ThemeService themeService = getFactory().getThemeService();

            bgColor = themeService.getTheme().getBackgroundColor();
            color = themeService.getTheme().getTextColor();

            optionsLayout.setFocusable( true );

            optionsScrollView.setFocusable( true );
            optionsScrollView.setBackgroundColor( bgColor );

            setupCurrency();

            themeSpinnerLabel.setTextColor( color );
            connectionTimeoutLabel.setTextColor( color );
            maxErrorsLabel.setTextColor( color );

            optionsLayout.addView( generateToggleButton( new ToggleSetting()
            {
                @Override
                public boolean get()
                {
                    return _configuration.getShowAdvertisements();
                }

                @Override
                public void set( boolean newValue )
                {
                    _configuration.setShowAdvertisements( newValue );
                }
            }, "Thanks for supporting MinerStatus!", "No ads for you!" ), 0 );
            optionsLayout.addView( generateTextViewForToggleButton( "Toggle Ad Visibility:" ), 0 );

            for ( final TickerMetadata tickerMetadata : Metadata.getTickerMetadatas( getFactory() ) )
            {
                final String key = tickerMetadata.getName();

                optionsLayout.addView( generateToggleButton( new ToggleSetting()
                {
                    @Override
                    public boolean get()
                    {
                        return _configuration.getShowExchange( key );
                    }

                    @Override
                    public void set( boolean newValue )
                    {
                        _configuration.setShowExchange( key, newValue );
                    }
                }, tickerMetadata.getLabel() + " Visible", tickerMetadata.getLabel() + " Hidden" ), 0 );
                optionsLayout.addView( generateTextViewForToggleButton( "Toggle " + tickerMetadata.getLabel()
                    + " Visibility:" ), 0 );
            }

            connectionTimeoutPicker.setCurrent( _configuration.getConnectionTimeout() );
            connectionTimeoutPicker.setOnChangeListener( new OnChangedListener()
            {
                @Override
                public void onChanged( NumberPicker picker, int oldVal, int newVal )
                {
                    if ( newVal < 0 )
                    {
                        newVal = 0;
                    }

                    _configuration.setConnectionTimeout( newVal );
                }
            } );

            maxErrorsPicker.setCurrent( _configuration.getMaximumErrors() );
            maxErrorsPicker.setOnChangeListener( new OnChangedListener()
            {
                @Override
                public void onChanged( NumberPicker picker, int oldVal, int newVal )
                {
                    if ( newVal < 0 )
                    {
                        newVal = MAX_ERRORS;
                    }

                    _configuration.setMaximumErrors( newVal );
                }
            } );

            String themeString = _configuration.getTheme();

            if ( themeString.equals( "light" ) )
            {
                radio_light.setChecked( true );
            }
            else if ( themeString.equals( "dark" ) )
            {
                radio_dark.setChecked( true );
            }

            OnClickListener radioListener = //
                new OnClickListener()
                {
                    public void onClick( View v )
                    {
                        _configuration.setTheme( v.getTag().toString() );
                    }
                };

            radio_dark.setTextColor( color );
            radio_dark.setTag( "dark" );
            radio_dark.setOnClickListener( radioListener );

            radio_light.setTextColor( color );
            radio_light.setTag( "light" );
            radio_light.setOnClickListener( radioListener );

            hashrateFormatCommasLabel.setTextColor( color );

            hashrateFormatCommasCheckBox.setChecked( _configuration.getHashrateFormatCommas() );
            hashrateFormatCommasCheckBox.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged( CompoundButton buttonView, boolean isChecked )
                {
                    _configuration.setHashrateFormatCommas( isChecked );
                }
            } );

            hashrateFormatDecimalsLabel.setTextColor( color );

            hashrateFormatDecimalsPicker.setCurrent( _configuration.getHashrateFormatDecimals() );
            hashrateFormatDecimalsPicker.setOnChangeListener( new OnChangedListener()
            {
                @Override
                public void onChanged( NumberPicker picker, int oldVal, int newVal )
                {
                    if ( newVal < 0 )
                    {
                        newVal = 0;
                    }

                    _configuration.setHashrateFormatDecimals( newVal );
                }
            } );

            hashrateFormatUnitsLabel.setTextColor( color );

            final HashrateListAdapter hashrateUnitsListAdapter =
                generateHashrateUnitsListAdapter( _configuration.getHashrateUnit() );
            ;

            hashrateFormatUnitsList.setAdapter( hashrateUnitsListAdapter );
            hashrateFormatUnitsList.collapseGroup( 0 );
            hashrateFormatUnitsList.setOnGroupExpandListener( new ExpandableListView.OnGroupExpandListener()
            {
                @Override
                public void onGroupExpand( int arg0 )
                {
                    int totalHeight = measureHeight( hashrateUnitsListAdapter.getGroupView( 0, false, null, null ) );

                    for ( int i = 0; i < hashrateUnitsListAdapter.getChildrenCount( 0 ); i++ )
                    {
                        totalHeight +=
                            1 + measureHeight( hashrateUnitsListAdapter.getChildView( 0, i, false, null, null ) );
                    }

                    hashrateFormatUnitsList.setLayoutParams( new LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT,
                                                                                            totalHeight ) );
                }
            } );
            hashrateFormatUnitsList.setOnGroupCollapseListener( new ExpandableListView.OnGroupCollapseListener()
            {
                @Override
                public void onGroupCollapse( int arg0 )
                {
                    int headerHeight = measureHeight( hashrateUnitsListAdapter.getGroupView( 0, false, null, null ) );

                    hashrateFormatUnitsList.setLayoutParams( new LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT,
                                                                                            headerHeight ) );
                }

            } );
            hashrateFormatUnitsList.setOnChildClickListener( new ExpandableListView.OnChildClickListener()
            {
                @Override
                public boolean onChildClick( ExpandableListView parent, View v, int groupPosition, int childPosition,
                                             long id )
                {
                    final HashrateList hashrateList = (HashrateList) hashrateUnitsListAdapter.getGroup( 0 );

                    HashrateUnit newSelectedHashrateUnit = hashrateList.getItemAt( childPosition );

                    // Check if this is the "Custom" entry.
                    if ( newSelectedHashrateUnit.getScale() < 0 )
                    {
                        // Yes it is.
                        HashrateUnit initialValue = hashrateList.getSelectedItem();

                        if ( initialValue.getIsAuto() || initialValue.getIsAutoForEach() )
                        {
                            initialValue = hashrateList.getItemAt( 0 );
                        }

                        CustomHashrateUnitActivity.showAsPopup( getFactory(), OptionsActivity.this, parent,
                                                                initialValue, //
                                                                new CustomHashrateUnitActivity.OnHashrateUnitChanged()
                                                                {
                                                                    @Override
                                                                    public void onHashrateUnitChanged( HashrateUnit newHashrateUnit )
                                                                    {
                                                                        hashrateList.setSelectedItem( newHashrateUnit );
                                                                        _configuration.setHashrateUnit( newHashrateUnit );
                                                                    }
                                                                }, //
                                                                new CustomHashrateUnitActivity.OnPopupDismissed()
                                                                {
                                                                    @Override
                                                                    public void onPopupDismissed()
                                                                    {
                                                                        hashrateFormatUnitsList.collapseGroup( 0 );
                                                                    }
                                                                } );
                    }
                    else
                    {
                        hashrateList.setSelectedItem( newSelectedHashrateUnit );
                        _configuration.setHashrateUnit( newSelectedHashrateUnit );

                        hashrateFormatUnitsList.collapseGroup( 0 );
                    }

                    return true;
                }
            } );

            hashrateUnitsListAdapter.setOnGroupClickListener( new HashrateListAdapter.OnGroupClickListener()
            {
                @Override
                public void onGroupClicked( View view, int groupPosition )
                {
                    if ( hashrateFormatUnitsList.isGroupExpanded( groupPosition ) )
                    {
                        hashrateFormatUnitsList.collapseGroup( groupPosition );
                    }
                    else
                    {
                        hashrateFormatUnitsList.expandGroup( groupPosition );
                    }
                }
            } );

            widgetSpinnerLabel.setTextColor( color );

            final int defaultWidgetSpinnerSelection =
                populateWidgetSpinner( widget_spinner, _configuration.getWidgetApiKey() );

            widget_spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener()
            {
                private boolean initialized = false;

                @Override
                public void onItemSelected( AdapterView<?> arg0, View arg1, int position, long arg3 )
                {
                    if ( !initialized )
                    {
                        widget_spinner.setSelection( defaultWidgetSpinnerSelection );
                        initialized = true;
                        return;
                    }

                    Spinner spinner = (Spinner) arg0;

                    @SuppressWarnings( "unchecked" )
                    ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) spinner.getAdapter();

                    String apiKey = arrayAdapter.getItem( position );

                    _configuration.setWidgetApiKey( apiKey );
                }

                @Override
                public void onNothingSelected( AdapterView<?> arg0 )
                {
                    _configuration.setWidgetApiKey( "" );
                }
            } );

            widgetThemeSpinnerLabel.setTextColor( color );

            String widgetThemeString = _configuration.getWidgetTheme();

            if ( widgetThemeString.equals( "light" ) )
            {
                radio_widget_light.setChecked( true );
            }
            else if ( widgetThemeString.equals( "dark" ) )
            {
                radio_widget_dark.setChecked( true );
            }

            OnClickListener widgetRadioListener = //
                new OnClickListener()
                {
                    public void onClick( View v )
                    {
                        _configuration.setWidgetTheme( v.getTag().toString() );
                    }
                };

            radio_widget_dark.setTextColor( color );
            radio_widget_dark.setTag( "dark" );
            radio_widget_dark.setOnClickListener( widgetRadioListener );

            radio_widget_light.setTextColor( color );
            radio_widget_light.setTag( "light" );
            radio_widget_light.setOnClickListener( widgetRadioListener );

            lowHashrateNotificationLabel.setTextColor( color );

            BigDecimal lowHashrateValue = _configuration.getLowHashrateNotification();

            if ( lowHashrateValue == null )
            {
                lowHashrateInput.setText( "off" );
            }
            else
            {
                lowHashrateInput.setText( lowHashrateValue.toString() );
            }

            lowHashrateInput.setOnFocusChangeListener( new View.OnFocusChangeListener()
            {
                @Override
                public void onFocusChange( View v, boolean hasFocus )
                {
                    EditText view = (EditText) v;

                    if ( hasFocus && ( _configuration.getLowHashrateNotification() == null ) )
                    {
                        view.setText( "" );
                    }
                }
            } );

            lowHashrateSaveButton.setOnClickListener( new View.OnClickListener()
            {
                @Override
                public void onClick( View v )
                {
                    BigDecimal configValue = null;

                    try
                    {
                        configValue = StatusBase.parseHashrate( lowHashrateInput.getText().toString() );

                        if ( configValue.compareTo( BigDecimal.ZERO ) <= 0 )
                        {
                            configValue = null;
                        }
                    }
                    catch ( NumberFormatException e )
                    { /* Leave value off */
                    }

                    String displayValue = "off";

                    if ( configValue != null )
                    {
                        displayValue = configValue.toString();
                    }

                    lowHashrateInput.setText( displayValue );

                    _configuration.setLowHashrateNotification( configValue );

                    Toast.makeText( getApplicationContext(), "Low Hashrate set to: " + displayValue, Toast.LENGTH_LONG ).show();
                }
            } );

            vibrateOnNotificationLabel.setTextColor( color );

            vibrateOnNotificationCheckBox.setChecked( _configuration.getVibrateOnNotification() );
            vibrateOnNotificationCheckBox.setOnCheckedChangeListener( new OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged( CompoundButton buttonView, boolean isChecked )
                {
                    _configuration.setVibrateOnNotification( isChecked );
                }
            } );

            deleteSpinnerLabel.setTextColor( color );

            populateDeleteSpinner( miner_delete_spinner );

            deleteMinerButtonOptionsMenu.setOnClickListener( new OnClickListener()
            {
                public void onClick( View v )
                {
                    if ( miner_delete_spinner.getSelectedItem() == null )
                    {
                        Toast.makeText( getApplicationContext(), "You cannot delete nothing!?  Or can you?",
                                        Toast.LENGTH_LONG ).show();
                        return;
                    }
                    AlertDialog.Builder alert = new AlertDialog.Builder( OptionsActivity.this );
                    alert.setTitle( "Remove " + miner_delete_spinner.getSelectedItem() + "?" );
                    alert.setPositiveButton( "Remove", new DialogInterface.OnClickListener()
                    {
                        public void onClick( DialogInterface dialog, int whichButton )
                        {
                            Toast.makeText( getApplicationContext(),
                                            miner_delete_spinner.getSelectedItem() + " removed.", Toast.LENGTH_LONG ).show();
                            getFactory().getMinerService().deleteMiner( ( miner_delete_spinner.getSelectedItem() ).toString() );
                            populateDeleteSpinner( miner_delete_spinner );
                        }
                    } );
                    alert.setNegativeButton( "Cancel", new DialogInterface.OnClickListener()
                    {
                        public void onClick( DialogInterface dialog, int whichButton )
                        {
                            dialog.cancel();
                        }
                    } );
                    alert.show();
                }
            } );
        }
        catch ( Exception e )
        {
            Toast.makeText( getApplicationContext(), "Error initializing options: " + e.toString(), Toast.LENGTH_LONG ).show();
        }
    }

    private void setupCurrency()
    {
        currencyLabel.setTextColor( color );
        ArrayAdapter<Currency> currencyAdapter =
            new ArrayAdapter<Currency>( this, android.R.layout.simple_spinner_item, Currency.values() );

        currencyListView.setAdapter( currencyAdapter );
        currencyListView.setSelection( currencyAdapter.getPosition( _configuration.getCurrency() ) );
        currencyListView.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected( AdapterView<?> adapterView, View view, int position, long l )
            {
                Spinner spinner = (Spinner) adapterView;

                @SuppressWarnings( "unchecked" )
                ArrayAdapter<Currency> arrayAdapter = (ArrayAdapter<Currency>) spinner.getAdapter();
                Currency currency = arrayAdapter.getItem( position );

                _configuration.setCurrency( currency );
            }

            @Override
            public void onNothingSelected( AdapterView<?> adapterView )
            {
                _configuration.setCurrency( Currency.USD );
            }
        } );
    }

    private static int measureHeight( View view )
    {
        view.measure( MeasureSpec.makeMeasureSpec( 0, MeasureSpec.UNSPECIFIED ),
                      MeasureSpec.makeMeasureSpec( 0, MeasureSpec.UNSPECIFIED ) );

        return view.getMeasuredHeight();
    }

    private HashrateListAdapter generateHashrateUnitsListAdapter( HashrateUnit selectedHashrateUnit )
    {
        ArrayList<HashrateUnit> options = new ArrayList<HashrateUnit>();

        options.add( new HashrateUnit( "H/s", 1, false, false ) );
        options.add( new HashrateUnit( "KH/s", 1000, false, false ) );
        options.add( new HashrateUnit( "MH/s", 1000000, false, false ) );
        options.add( new HashrateUnit( "GH/s", 1000000000, false, false ) );
        options.add( new HashrateUnit( "Auto Pick", 0, true, false ) );
        options.add( new HashrateUnit( "Auto Pick For Each", 0, false, true ) );
        options.add( new HashrateUnit( "Custom...", -1, false, false ) );

        HashrateList list = new HashrateList();

        list.setItems( options );
        list.setSelectedItem( selectedHashrateUnit );

        return new HashrateListAdapter( this, list );
    }

    private void populateDeleteSpinner( Spinner spinner )
    {
        Cursor cur = getFactory().getMinerService().getMiners();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_spinner_item );

        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        while ( cur.moveToNext() )
        {
            String miner = cur.getString( 0 );

            adapter.add( miner );
        }

        spinner.setAdapter( adapter );
        cur.close();
    }

    private int populateWidgetSpinner( Spinner spinner, String defaultSelection )
    {
        Cursor cur = getFactory().getMinerService().getMiners();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_spinner_item );

        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        adapter.add( "none" );

        int position = 0;
        int i = 1;

        while ( cur.moveToNext() )
        {
            String miner = cur.getString( 0 );

            adapter.add( miner );

            if ( miner.equals( defaultSelection ) )
            {
                position = i;
            }

            i++;
        }

        spinner.setAdapter( adapter );
        cur.close();

        return position;
    }
}
