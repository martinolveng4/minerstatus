package com.trumpetx.minerstatus.widget;

import com.trumpetx.minerstatus.R;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

// Based on: http://stackoverflow.com/questions/9713669/auto-scale-text-size
//      and: http://stackoverflow.com/questions/2695646/declaring-a-custom-android-ui-element-using-xml
//      and: https://github.com/android/platform_frameworks_base/blob/master/core/java/android/widget/TextView.java

public class AutoFitTextView
    extends TextView
{
    static final String TAG = "AFTV";

    public AutoFitTextView( Context context )
    {
        super( context );

        Log.d( TAG, "AutoFitTextView(context)" );
    }

    public AutoFitTextView( Context context, AttributeSet attrs )
    {
        super( context, attrs );

        Log.d( TAG, "AutoFitTextView(context, attrs)" );

        init( context, attrs );
    }

    public AutoFitTextView( Context context, AttributeSet attrs, int defStyle )
    {
        super( context, attrs, defStyle );

        Log.d( TAG, "AutoFitTextView(context, attrs, defStyle)" );

        init( context, attrs );
    }

    protected void init( Context context, AttributeSet attrs )
    {
        Log.d( TAG, "init: getting TypedArray of styled attributes" );

        TypedArray attrArray = context.obtainStyledAttributes( attrs, R.styleable.AutoFitTextView );

        try
        {
            Log.d( TAG, "init: getting min and max text size attribute values" );

            _minTextSize = attrArray.getDimension( R.styleable.AutoFitTextView_minTextSize, -1f );
            _maxTextSize = attrArray.getDimension( R.styleable.AutoFitTextView_maxTextSize, -1f );

            Log.d( TAG, "init: values: min=" + _minTextSize + ", max=" + _maxTextSize );
        }
        finally
        {
            attrArray.recycle();
        }
    }

    float _minTextSize = -1f;

    float _maxTextSize = -1f;

    public float getMinTextSize()
    {
        return _minTextSize;
    }

    public void setMinTextSize( float newValue )
    {
        setMinTextSize( TypedValue.COMPLEX_UNIT_SP, newValue );
    }

    public void setMinTextSize( int unit, float newValue )
    {
        Context c = getContext();
        Resources r;

        if ( c == null )
            r = Resources.getSystem();
        else
            r = c.getResources();

        _minTextSize = TypedValue.applyDimension( unit, newValue, r.getDisplayMetrics() );

        if ( getTextSize() < _minTextSize )
            setTextSize( _minTextSize );
    }

    public float getMaxTextSize()
    {
        return _maxTextSize;
    }

    public void setMaxTextSize( float newValue )
    {
        setMaxTextSize( TypedValue.COMPLEX_UNIT_SP, newValue );
    }

    public void setMaxTextSize( int unit, float newValue )
    {
        Context c = getContext();
        Resources r;

        if ( c == null )
            r = Resources.getSystem();
        else
            r = c.getResources();

        _maxTextSize = TypedValue.applyDimension( unit, newValue, r.getDisplayMetrics() );

        if ( getTextSize() > _maxTextSize )
            setTextSize( _maxTextSize );
    }

    protected void refitText( String text, int textWidth )
    {
        Log.d( TAG, "refitText: fitting \"" + text + "\" into " + textWidth + " pixels" );

        if ( textWidth > 0 )
        {
            int padding = getPaddingLeft() + getPaddingRight();
            int availableWidth = textWidth - padding;

            Log.d( TAG, "refitText: padding=" + padding + ", available width=" + availableWidth );

            float minSize = _minTextSize;
            float maxSize = _maxTextSize;

            if ( minSize < 1 )
                minSize = 1;

            if ( maxSize < 1 )
                maxSize = 1000;

            Log.d( TAG, "refitText: search boundaries: " + minSize + " to " + maxSize );

            if ( minSize >= maxSize )
                setTextSize( TypedValue.COMPLEX_UNIT_PX, minSize );
            else
            {
                TextPaint paint = getPaint();

                float trySize = maxSize;

                while ( maxSize - minSize > 0.5f )
                {
                    trySize = ( minSize + maxSize ) * 0.5f;

                    Log.d( TAG, "refitText: trying " + trySize );

                    setTextSize( TypedValue.COMPLEX_UNIT_PX, trySize );

                    boolean fits = ( paint.measureText( text ) <= availableWidth );

                    if ( fits )
                        minSize = trySize;
                    else
                        maxSize = trySize;
                }

                Log.d( TAG, "refitText: search ended on " + trySize + ", polishing" );

                while ( ( trySize > minSize ) && ( paint.measureText( text ) > availableWidth ) )
                {
                    trySize -= 0.5f;
                    setTextSize( TypedValue.COMPLEX_UNIT_PX, trySize );
                }

                Log.d( TAG, "refitText: final text size value: " + trySize );
            }
        }
    }

    @Override
    protected void onTextChanged( CharSequence text, int start, int lengthBefore, int lengthAfter )
    {
        Log.d( TAG, "onTextChanged(text: \"" + text + "\", start: " + start + ", lengthBefore: " + lengthBefore
            + ", lengthAfter: " + lengthAfter + ")" );

        super.onTextChanged( text, start, lengthBefore, lengthAfter );

        refitText( text.toString(), this.getWidth() );
    }

    @Override
    protected void onSizeChanged( int w, int h, int oldw, int oldh )
    {
        Log.d( TAG, "onSizeChanged(w: " + w + ", h: " + h + ", oldw: " + oldw + ", oldh: " + oldh + ")" );

        super.onSizeChanged( w, h, oldw, oldh );

        if ( w != oldw )
            refitText( getText().toString(), w );
    }
}
