package com.trumpetx.minerstatus.currency;

import static com.trumpetx.minerstatus.util.Configuration.*;

public enum Currency
{
    USD( "US Dollar" ),
    GBP( POUND, "Great British Pound" ),
    EUR( EURO, "Euro" ),
    JPY( YEN, "Japanese Yen" ),
    AUD( "Australian Dollar" ),
    CAD( "Canadian Dollar" ),
    CHF( SWISS_FRANC, "Swiss Franc" ),
    CNY( YEN, "Chinese Yuan" ),
    DKK( KRONE, "Danish Krone" ),
    HKD( "Hong Kong Dollar" ),
    PLN( ZLOTY, "Polish Złoty" ),
    RUB( RUBLE, "Russian Rouble" ),
    SEK( KRONE, "Swedish Krona" ),
    SGD( "Singapore Dollar" ),
    THB( BAHT, "Thai Baht" );

    private String description;

    private String label;

    private Currency( String description )
    {
        this( DOLLAR, description );
    }

    private Currency( String label, String description )
    {
        this.label = label;
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public String getLabel()
    {
        return label;
    }

    public String getKey()
    {
        return name();
    }

    @Override
    public String toString()
    {
        return getDescription();
    }
}
